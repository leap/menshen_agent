# syntax=docker/dockerfile:1
# Dockerfile.distroless

##
## Build
##
FROM golang:1.18-bullseye as base

WORKDIR $GOPATH/src/0xacab.org/leap/menshen_agent/

COPY . .

RUN go mod download
RUN go mod verify

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /menshen_agent ./cmd/menshen_agent 

##
## Deploy
##
FROM scratch

COPY --from=base /menshen_agent .

CMD ["./menshen_agent"]

