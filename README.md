# Menshen agent

The agent for the [menshen](https://0xacab.org/leap/menshen) service.

This agent needs to run on each gateway (and, eventually, bridges) and collects metrics related with congestion, which are reported to menshen via `gRPC`.

* load
* fullness

## Usage

It needs:

* Access to the openvpn exporter to curl /metrics.
* Access to the following files:
  * `/proc/net/dev`
  * `/proc/sys/net/netfilter/nf_conntrack_count`, and 
  * `/proc/sys/net/netfilter/nf_conntrack_max`.
* Access to menshen balancer port to send the metrics. It is assumed that the
  gRPC port does not need authentication.

The configuration is done via environment variables:

```bash
❯ ./menshen_agent -h
This application is configured via the environment. The following environment
variables can be used:

KEY                     TYPE                DEFAULT                          REQUIRED    DESCRIPTION
AGENT_MENSHENADDRESS    String              localhost:9003                               domain:port of the menshen balancer listener
AGENT_NAME              String                                               true        gateway name, it must be different on each gateway
AGENT_NETDEVICE         String              eth0                                         network device to monitor
AGENT_BANDWIDTH         Float                                                            bandwidth available in bytes
AGENT_TXBANDWIDTH       Float                                                            transmit bandwidth, if not provided it will use BANDWIDTH
AGENT_RXBANDWIDTH       Float                                                            recieve bandwidth, if not provided it will use BANDWIDTH
AGENT_PACKETS           Float               100_000                                      max packets per second that the gateway can sustain
AGENT_TXPACKETS         Float                                                            max transmit packets per second, if not provided it will use PACKETS
AGENT_RXPACKETS         Float                                                            max receive packets per second, if not provided it will use PACKETS
AGENT_CLIENTS           Unsigned Integer    200                                          max number of clients that the gateway can sustain
AGENT_OVPNEXPORTER      String              http://localhost:9176/metrics                where to HTTP GET the openvpn exporter metrics
AGENT_LOCATION          String                                               true        location city of the gateway
```

`AGENT_NAME` has to be an unique identifier for each gateway or bridge. This will be the key with wich the load balancer will index the reported utilization metrics.
