// A load simulation to better understand and calibrate
// different metrics.
// You can use this to simulate a variation in the number of connected OpenVPN
// Clients.
package main

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/aquilax/go-perlin"
)

const (
	connstr    = `openvpn_connections{server="huayna"} %d`
	maxClients = 200
)

var (
	// parameters for perlin noise generation
	alpha       = 5.
	beta        = 2.
	n     int32 = 5
)

func getMetricsHandler(ch chan float64) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		val := int(maxClients * <-ch)
		fmt.Printf("GET /metrics: %v\n", val)
		io.WriteString(w, fmt.Sprintf(connstr, val))
	}
}

func main() {
	c := make(chan float64, 1)
	generateNoise(c, 50)
	http.HandleFunc("/metrics", getMetricsHandler(c))
	log.Fatal(http.ListenAndServe(":9176", nil))
}

func generateNoise(ch chan float64, smooth int) {
	seed := time.Now().UnixNano()
	p := perlin.NewPerlinRandSource(alpha, beta, n, rand.NewSource(seed))

	go func() {
		x := 0.0
		for {
			// noise distribution is symmetrically centered in the
			// (0,1) interval
			ch <- p.Noise1D(x/float64(smooth)) + 0.5
			x += 1
		}
	}()
}
