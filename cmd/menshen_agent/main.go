// Copyright (c) 2021 LEAP Encryption Access Project
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"os"
	"strings"
	"time"

	lbagent "git.autistici.org/ale/lb/agent"
	lbpb "git.autistici.org/ale/lb/proto"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"0xacab.org/leap/menshen_agent/pkg/collector"
)

type Config struct {
	Bandwidth      float64 `desc:"bandwidth available in bytes"`
	Debug          bool    `default:"false" desc:"set verbose logging"`
	Interval       uint    `default:"10" desc:"Interval for pushing values to load balancer, in seconds"`
	Location       string  `required:"true" desc:"location city of the gateway"`
	MaxClients     uint    `default:"200" desc:"max number of clients that the gateway can sustain"`
	MenshenAddress string  `default:"localhost:9003" desc:"domain:port of the menshen balancer listener"`
	Name           string  `required:"true" desc:"gateway name, it must be different on each gateway"`
	NetDevice      string  `default:"eth0" desc:"network device to monitor"`
	OvpnExporter   string  `default:"http://localhost:9176/metrics" desc:"where to HTTP GET the openvpn exporter metrics"`
}

var (
	// These are all the available metrics. While we experiment and optimize
	// with a combination of them, it's useful to have an easy toggle switch,
	// even if it's at compile time.

	shouldEnableConntrackGauge = true
	shouldEnableClientCounter  = true
	shouldEnableBandwithGauge  = true
	shouldEnablePacketGauge    = true
)

func registerConntrack(a *lbagent.Agent, _ Config) {
	a.RegisterUtilization(
		"conntrack",
		lbpb.Utilization_GAUGE,
		collector.ConntrackUtilization)
}

func registerClientUtilization(a *lbagent.Agent, c Config) {
	a.RegisterUtilization(
		"clients",
		lbpb.Utilization_GAUGE,
		collector.GetConnectedClients(c.OvpnExporter, c.MaxClients),
	)
}

func registerBandwidth(a *lbagent.Agent, c Config) {
	tx := collector.GetTxByteRateCounter(c.NetDevice)
	a.RegisterUtilization(
		"bandwidth_tx",
		lbpb.Utilization_GAUGE, tx.Rate)

	rx := collector.GetRxByteRateCounter(c.NetDevice)
	a.RegisterUtilization(
		"bandwidth_rx",
		lbpb.Utilization_GAUGE,
		rx.Rate)
}

func registerPackets(a *lbagent.Agent, c Config) {
	tx := collector.GetTxPacketsRateCounter(c.NetDevice)
	a.RegisterUtilization(
		"packets_tx",
		lbpb.Utilization_GAUGE, tx.Rate)

	rx := collector.GetRxPacketsRateCounter(c.NetDevice)
	a.RegisterUtilization(
		"packets_rx",
		lbpb.Utilization_GAUGE,
		rx.Rate)
}

func main() {
	if len(os.Args) > 1 {
		envconfig.Usage("agent", &Config{})
		return
	}

	var config Config
	envconfig.MustProcess("agent", &config)

	log.SetFormatter(&log.JSONFormatter{})
	if config.Debug {
		log.SetLevel(log.DebugLevel)
	}

	intervalSeconds := time.Duration(config.Interval) * time.Second

	dialer, err := lbagent.NewGRPCDialer(config.MenshenAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Can't start the grpc dialer: %v", err)
	}

	// tags is a map of tags that we will send to the load balancer to track
	// the metrics coming from this node. For now we're only using the location tag.
	tags := map[string]string{
		"location": strings.ToLower(config.Location),
	}

	// initialize the load balancer agent.
	agent := lbagent.New(config.Name, tags, dialer)

	// First metric: conntrack ratio (how far is this server from the
	// maximum numbers of flows netfilter can track)
	if shouldEnableConntrackGauge {
		registerConntrack(agent, config)
	}

	// Second metric: openvpn client number ratio (connected clients
	// divided by the maximum allowed number of connected clients)
	if shouldEnableClientCounter {
		registerClientUtilization(agent, config)
	}

	// Third metric: ratio of bandwidth usage (rx and tx)
	if shouldEnableBandwithGauge {
		registerBandwidth(agent, config)
	}

	// Fourth metric: ratio of packets on the egress interface (rx and tx)
	if shouldEnablePacketGauge {
		registerPackets(agent, config)
	}

	// TODO register CPU utilization, which can be tricky on multi-core.
	// Need to track process for each core.

	agent.SetUpdateInterval(intervalSeconds)
	agent.SetReady(true)
	agent.Start()

	defer agent.Stop()

	select {}
}
