module 0xacab.org/leap/menshen_agent

go 1.15

require (
	git.autistici.org/ale/lb v0.0.0-20210301105648-288f2f5853b7
	github.com/RobinUS2/golang-moving-average v1.0.0
	github.com/aquilax/go-perlin v1.1.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/procfs v0.11.1
	github.com/sirupsen/logrus v1.9.3
	google.golang.org/grpc v1.36.0
)
