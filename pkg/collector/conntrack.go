// Copyright (c) 2020 ale https://git.autistici.org/ale/lb/
// Copyright (c) 2021 LEAP Encryption Access Project
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package collector

import (
	"bytes"
	"io/ioutil"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// ConntrackUtilization returns the ratio between the current number of flow
// currently tracked by the netfilter subsystem and the maximum number of flows that are configured.
// We might want to penalize getting close to the unit (i.e., we could use a multiplier so that
// the load balancer avoids picking this node with a greater weight).
func ConntrackUtilization() float64 {
	count, err := readNumber("/proc/sys/net/netfilter/nf_conntrack_count")
	if err != nil {
		log.Printf("Can't read the conntrack count: %v", err)
		return 0
	}

	max, err := readNumber("/proc/sys/net/netfilter/nf_conntrack_max")
	if err != nil {
		log.Printf("Can't read the conntrack max: %v", err)
		return 0
	}

	conntrack_ratio := float64(count) / float64(max)
	log.Debugf("Conntrack: %.5f", conntrack_ratio)
	return conntrack_ratio
}

func readNumber(path string) (uint64, error) {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return 0, err
	}
	content = bytes.TrimSpace(content)
	return strconv.ParseUint(string(content), 10, 64)
}
