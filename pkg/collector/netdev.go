package collector

import (
	"fmt"
	"reflect"

	"github.com/prometheus/procfs"
)

// netNetDev gets a single line from /proc/net/dev
func getNetDev(dev string) procfs.NetDevLine {
	p, err := procfs.Self()
	if err != nil {
		fmt.Println(err)
		return procfs.NetDevLine{}
	}
	n, err := p.NetDev()
	if err != nil {
		fmt.Println(err)
		return procfs.NetDevLine{}
	}
	netdev := n[dev]
	return netdev
}

func getCounterByName(dev, field string) *RateCounter {
	return NewRateCounter(
		10,
		func() float64 {
			nd := reflect.Indirect(reflect.ValueOf(getNetDev(dev)))
			return float64(nd.FieldByName(field).Interface().(uint64))
		},
	)
}

func GetTxByteRateCounter(dev string) *RateCounter {
	return getCounterByName(dev, "TxBytes")
}

func GetRxByteRateCounter(dev string) *RateCounter {
	return getCounterByName(dev, "RxBytes")
}

func GetTxPacketsRateCounter(dev string) *RateCounter {
	return getCounterByName(dev, "TxPackets")
}

func GetRxPacketsRateCounter(dev string) *RateCounter {
	return getCounterByName(dev, "RxPackets")
}
