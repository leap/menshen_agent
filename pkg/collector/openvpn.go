// Copyright (c) 2021-2023 LEAP Encryption Access Project
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package collector

import (
	"bufio"
	"net/http"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

const (
	openvpnExporterClients = "openvpn_connections"
)

var (
	openVPNClientTrackEnabled = true
)

// GetConnectedClients will fetch the passed url `exporter`, which is expected to be
// the standard report from the openvpn prometheus exporter. From there, it will fetch the line
// delimited by `openvpnExporterClients`, and return the ratio between the
// current number and the maximum configured number of clients.
func GetConnectedClients(exporter string, max uint) func() float64 {
	_, err := http.Get(exporter)
	if err != nil {
		log.Printf("Disabling OpenVPN Client track. Exporter down? error: %v", err)
		openVPNClientTrackEnabled = false
	}
	return func() float64 {
		if !openVPNClientTrackEnabled {
			return 0
		}
		resp, err := http.Get(exporter)
		if err != nil {
			log.Printf("Error getting from the exporter: %v", err)
			return 0
		}
		defer resp.Body.Close()

		scanner := bufio.NewScanner(resp.Body)
		for scanner.Scan() {
			line := scanner.Text()
			if strings.HasPrefix(line, openvpnExporterClients) {
				clients := strings.Split(line, " ")[1]
				num, err := strconv.Atoi(clients)
				if err != nil {
					log.Errorf("Error parsing number of clients %s: %v", clients, err)
				} else {
					ratio := float64(num) / float64(max)
					log.Debugf("Client track: %v", ratio)
					return ratio
				}
			}
		}
		return 0
	}
}
