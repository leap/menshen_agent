package collector

import (
	"time"

	avg "github.com/RobinUS2/golang-moving-average"
)

var (
	// runningBufferLength is how many measurements we keep in the running average
	// TODO should experiment a little bit about sensitivity when changing the window value
	// A good idea would be to store a data every 5 min or so in a different counter, so that
	// we can keep a high-water mark for the day and make the instantaneus
	// rate compute against that.
	// TODO specify sampling period (for each measurement and for High
	// Water Mark: it makes sense that we consider a daily cycle for reporting the max)
	runningBufferLength int = 10

	// sampleIntervalSeconds is the period between to reads to take a single sample
	sampleIntervalSeconds = 2.
)

// RateCounter keeps track of counter values, and offers a dampened running
// average as a rate value that can be used to estimate the (relative) capacity
// of the given metric.
type RateCounter struct {
	ma     *avg.MovingAverage
	ticker *time.Ticker
	done   chan bool
	fn     func() float64
}

// NewRateCounter is the constructor for a RateCounter. It expects the sampling
// interval (in ms), and the function to sample a value.
func NewRateCounter(ms int, fn func() float64) *RateCounter {
	ma := avg.New(runningBufferLength)
	ma.SetIgnoreInfValues(true)
	ma.SetIgnoreNanValues(true)

	ticker := time.NewTicker(
		time.Millisecond * time.Duration(ms))

	rc := &RateCounter{
		ma:     ma,
		ticker: ticker,
		done:   make(chan bool),
		fn:     fn,
	}
	go rc.start()
	return rc
}

// Avg returns the running average for all the sampled values in the buffer.
func (rc *RateCounter) Avg() float64 {
	return rc.ma.Avg()
}

// Rate returns the current average divided by the maximum.
func (rc *RateCounter) Rate() float64 {
	mx, err := rc.ma.Max()
	if err != nil {
		return 0
	}
	return rc.Avg() / mx
}

// sample executes the passed function two times, waiting a fixed amount of
// time, and returns the difference divided by the elapsed period.
func sample(fn func() float64) float64 {
	x0 := fn()
	time.Sleep(time.Second * time.Duration(sampleIntervalSeconds))
	x1 := fn()
	return (x1 - x0) / float64(sampleIntervalSeconds)
}

// start starts the sampling loop
func (rc *RateCounter) start() {
	for {
		select {
		case <-rc.done:
			return
		case <-rc.ticker.C:
			rc.ma.Add(sample(rc.fn))
		}
	}
}
